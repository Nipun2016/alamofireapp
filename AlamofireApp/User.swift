//
//  User.swift
//  AlamofireApp
//
//  Created by BS23 on 5/25/17.
//  Copyright © 2017 edu. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper

class User {
    
    var  id = String()
    var name = String()
    var email = String()
    var address = String()
    var gender = String()
    var phone = UserPhone.self
    
    
    
    init(UserJSON : JSON) {
        
        //self.name = UserJSON["name"].stringValue
    
        name = UserJSON["name"].stringValue
        email = UserJSON["email"].stringValue
        address = UserJSON["address"].stringValue
        gender = UserJSON ["gender"].stringValue
        
    //  phone = UserPhone(UserPhoneJSON : UserJSON ["phone"])
    //  self.phone = UserPhone(UserPhoneJSON: UserJSON["phone"])
    //  self.phone = UserPhone.init(UserPhoneJSON: UserJSON["phone"])
    //  self.phone = UserPhone(UserPhoneJSON : UserJSON ["phone"])
    }
    
}

class UserPhone {
    
    var mobile = String()
    var home = String()
    var office = String()
    
    init(UserPhoneJSON : JSON ) {
        mobile = UserPhoneJSON["mobile"].stringValue
        home = UserPhoneJSON["home"].stringValue
        office = UserPhoneJSON ["office"].stringValue
    }
    
}
