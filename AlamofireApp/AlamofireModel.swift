//
//  AlamofireModel.swift
//  AlamofireApp
//
//  Created by BS23 on 5/24/17.
//  Copyright © 2017 edu. All rights reserved.
//

import UIKit
import Alamofire


class AlamofireModel: SessionManager {
    
    let urlString : String = "http://api.androidhive.info"
    
    let baseUrl = URL(string : "http://api.androidhive.info")

    static let manager: AlamofireModel  = {
        
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = AlamofireModel.defaultHTTPHeaders
        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        
        return AlamofireModel(configuration: configuration)
    }()
    
    override func request(_ url: URLConvertible, method: HTTPMethod, parameters: Parameters?, encoding: ParameterEncoding, headers: HTTPHeaders?) -> DataRequest {
        
        
        let aSuffix : String = "/contacts"
        let  url = urlString + aSuffix
        print(url)
        return super.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers)
    }

    
    
//    func getData()
//    {
//        
//        let task = URLSession.shared.dataTask(with: baseUrl!) { (data, response, error) in
//            if error != nil
//            {
//                print("Error")
//            }
//            else
//            {
//                if let contant = data
//                {
//                    do
//                    {
//                        let myJson = try JSONSerialization.jsonObject(with: contant, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
//                        //print(myJson)
//                        
//                        let data = myJson["rates"] as AnyObject
//                        print(data)
//                    }
//                    catch
//                    {
//                        
//                    }
//                }
//            }
//        }
//        task.resume()
//    }
//    
//    func postData()
//    {
//        
//    }
    
}
